<?php
/**
 * Created by PhpStorm.
 * User: steven
 * Date: 26/05/15
 * Time: 19:54
 */

class BabeRotator {

  function getRandomBabe() {

    $settings = $this->getSettings();

    $files = scandir($settings->folder);
    unset($files[0]);
    unset($files[1]);
    $files = array_values($files);

    $babe = $files[array_rand($files, 1)];

    return $settings->folder . DIRECTORY_SEPARATOR . $babe;
  }

  function getSettings() {

      if (file_exists('settings.json')) {
          $settings = json_decode(file_get_contents('settings.json'));
      } else {
          // default
          $settings = array('folder' => 'babes');
      }
      return $settings;
  }

  function saveSettings($settings) {
      file_put_contents('settings.json', json_encode($settings));
  }

} 