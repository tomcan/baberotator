<?php

require_once('BabeRotator.php');

$baberotator = new BabeRotator();

$settings = $baberotator->getSettings();

if (isset($_GET['to'])) {
    $settings->folder = $settings->folderbase . $_GET["to"];
    $baberotator->saveSettings($settings);
    header('Location: switch.php?r='.time());
}

?>
<html>
<head>
  <link rel="stylesheet" href="css/reset.css" />
  <link rel="stylesheet" href="css/style.css" />
  <script type="application/javascript" src="js/jquery-2.1.4.min.js"></script>
  <script type="application/javascript" src="js/scripts.js"></script>
</head>
<body>

    Current folder: <?php echo $settings->folder; ?>
    <br />

<?php

    $dirs = scandir(realpath($settings->folderbase));
    unset($dirs[0]);
    unset($dirs[1]);
    $dirs = array_values($dirs);

    foreach ($dirs as $dir) {
        echo '<br><a href="switch.php?to='. $dir .'">'. $dir . '</a>';
    }

?>

</body>
</html>